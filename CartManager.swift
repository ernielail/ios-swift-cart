//
//  CartManager.swift
//  Wrath and Grace
//
//  Created by Ernie Lail on 9/15/18.
//  Copyright © 2018 Development. All rights reserved.
//

import UIKit

class CartManager: NSObject {

    //set up singleton
    static let shared = CartManager()
    
    
// contains the products that have been added to the cart
    var productsArray = Array<ProductObject>()

    
    
//cart functions
    

    //empties all products from cart
    func emptyCart() {
        productsArray.removeAll(keepingCapacity: false)
    }
    //adds a product to the cart
    func addToCart(product: ProductObject) {
        productsArray.append(product)
    }
    //removes a product from the cart
    func removeFromCart(product: ProductObject) -> Bool {
        if let index = self.productsArray.index(of: product) {
            productsArray.remove(at: index)
            return true
        }
        return false
    }
    
    //returns how many items are in the cart
    func cartCount() -> Int {
        return productsArray.count
    }
    
    //calculates the total price of the items in the cart
    func totalPriceInCart() -> Double {
        var totalPrice: Double = 0.0
        for product in self.productsArray {
            totalPrice += product.totalPrice()
        }
        return totalPrice
    }
    
    
    
    
    
    

    //array to hold product categories (which contain products)
    var productCategories = [ProductCategories]()
    

    private override init() {
        
        //create all the products and add to corresponding category
        
        //if dynamic products, load from URL Request
        
        
        
        //-----------------New Cat-------------------

        
        
        //setup category
        let apparelCategory = ProductCategories()
        apparelCategory.title = "Apparel"
        apparelCategory.image = "Mens Style.jpg"
        apparelCategory.textColor = UIColor.gray
        apparelCategory.viewController = "CategoryListing"
        
        
        //for loop to add products
        //now add products
        let apparel1 = ProductObject()
        apparel1.title = "title test"
        apparel1.desc = "description test"
        var imagesArray = [String]()
        imagesArray.append("reformed_bars_red.png")
        apparel1.images = imagesArray
        var optionsArray = [String:Double]()
        optionsArray["option1"] = 0.0
        apparel1.options = optionsArray
        apparel1.optionType = "type"

        //now append to array
        
        apparelCategory.productsArray.append(apparel1)
        
        
        productCategories.append(apparelCategory)
      
        
        
        
        
        //-----------------New Cat-------------------
        
        
        //setup category
        let booksCategory = ProductCategories()
        booksCategory.title = "Books"
        //Books Background.jpg
        booksCategory.image = "Books Background.jpg"
        booksCategory.textColor = UIColor.white
        booksCategory.viewController = "CategoryListing"
        
        
        //for loop to add products
        //now add products
        let book1 = ProductObject()
        book1.title = "book test"
        book1.desc = "description test"
        var bookImagesArray = [String]()
        bookImagesArray.append("https://static.wixstatic.com/media/5b202e_5a96c4a195a5405bbcc94eee7e1e97d5~mv2_d_3647_2736_s_4_2.png/v1/fill/w_734,h_551,q_85,usm_0.66_1.00_0.01/5b202e_5a96c4a195a5405bbcc94eee7e1e97d5~mv2_d_3647_2736_s_4_2.png")
        print()
        book1.images = bookImagesArray
        var bookOptionsArray = [String:Double]()
        bookOptionsArray["option1"] = 0.0
        book1.options = optionsArray
        book1.optionType = "type"
        
        //now append to array
        booksCategory.productsArray.append(book1)
        
        
        productCategories.append(booksCategory)
        
        
        
        //-----------------New Cat-------------------

        
        //setup category
        let musicCategory = ProductCategories()
        musicCategory.title = "Music"
        musicCategory.image = "music.jpg"
        musicCategory.textColor = UIColor.white
        musicCategory.viewController = "CategoryListing"
        
        //for loop to add products
        //now add products
        let music1 = ProductObject()
        music1.title = "music test"
        music1.desc = "description test"
        var musicImagesArray = [String]()
        musicImagesArray.append("reformed_bars_red.png")
        music1.images = musicImagesArray
        var musicOptionsArray = [String:Double]()
        musicOptionsArray["option1"] = 0.0
        music1.options = optionsArray
        music1.optionType = "type"
        
        //now append to array
        
        musicCategory.productsArray.append(music1)
        
        
        
        productCategories.append(musicCategory)
        
        
        
        //-----------------New Cat-------------------

        
        //setup category
        let podcastCategory = ProductCategories()
        podcastCategory.title = "Podcast"
        podcastCategory.image = "podcast.jpg"
        podcastCategory.textColor = UIColor.gray
        podcastCategory.viewController = "CategoryListing"
        
        //for loop to add products
        //now add products
        let podcast1 = ProductObject()
        podcast1.title = "title test"
        podcast1.desc = "description test"
        var podcastImagesArray = [String]()
        podcastImagesArray.append("reformed_bars_red.png")
        podcast1.images = podcastImagesArray
        var podcastOptionsArray = [String:Double]()
        podcastOptionsArray["option1"] = 0.0
        podcast1.options = optionsArray
        podcast1.optionType = "type"
        
        //now append to array
        
        podcastCategory.productsArray.append(podcast1)
        
        
        productCategories.append(podcastCategory)
        
        
        
        //-----------------New Cat-------------------

        
        
        //setup category
        let contactCategory = ProductCategories()
        contactCategory.title = "Contact"
        contactCategory.image = "podcast.jpg"
        contactCategory.textColor = UIColor.white
        contactCategory.viewController = "CategoryListing"

        //for loop to add products
        //now add products
        let contact1 = ProductObject()
        contact1.title = "title test"
        contact1.desc = "description test"
        var contactImagesArray = [String]()
        contactImagesArray.append("reformed_bars_red.png")
        contact1.images = contactImagesArray
        var contactOptionsArray = [String:Double]()
        contactOptionsArray["option1"] = 0.0
        contact1.options = optionsArray
        contact1.optionType = "type"
        
        //now append to array
        
        contactCategory.productsArray.append(contact1)
        
        
        productCategories.append(contactCategory)

    }
    
    
    
    

    
}


