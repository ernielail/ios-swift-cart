//
//  ProductCategories.swift
//  Wrath and Grace
//
//  Created by Ernie Lail on 9/16/18.
//  Copyright © 2018 Development. All rights reserved.
//

import UIKit

class ProductCategories: NSObject {

    //category title
    var title:String?
    
    //background image for category
    var image:String?
    
    //viewController's Storyboard ID
    var viewController:String?
    
    //Color of the text that will overlay the photo
    var textColor:UIColor?
    
    //array of the products that fit in this category
    var productsArray = Array<ProductObject>()

    

    //function to add a prouct to the category
    func addProduct(product: ProductObject) {
        productsArray.append(product)
    }
    
    
    //function to remove a prouct to the category
    func removeProduct(product: ProductObject) -> Bool {
        if let index = self.productsArray.index(of: product) {
            productsArray.remove(at: index)
            return true
        }
        return false
    }
    
    
    
}
