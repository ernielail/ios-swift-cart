//
//  ProductObject.swift
//  Wrath and Grace
//
//  Created by Ernie Lail on 9/15/18.
//  Copyright © 2018 Development. All rights reserved.
//

import UIKit

class ProductObject: NSObject {

    //product title
    var title:String?
    
    //product description
    var desc:String?
    
    //array of strings that are either local file names or URLs
    var images = [String]()
    

    //hold the selected option name and price.
    private var selectedOptionName:String = ""
    private var selectedOptionPrice:Double?

    //for audio based products.
    var audioFiles = [[String:String]]()

    //Dictionary of the different options that will be available.
    //String should be the title and Double the price
    var options = [String:Double]()
    
    //label of the available options (i.e. "Size").
    var optionType:String = ""
    
    
    
    //returns total price for the item, including any selected option
    func totalPrice()->Double {
       var finalPrice = 0.0
        
        if let price = self.selectedOptionPrice {
            finalPrice = Double(price)
        }

        return finalPrice
    }

    //selects option, name must match exactly to String Key in options Dictionary
    func selectOption(name:String){
        self.selectedOptionName = name
        self.selectedOptionPrice = options[name]
    }
    
    //returns selected option name
    func returnSelectedOption()->String{
        var returnStr = "none"
        if self.selectedOptionName != "" {
            returnStr = self.selectedOptionName
        }
        return returnStr
    }
    
    //returns option type (for use in a label)
    func returnOptionType() ->String{
        var returnStr = "none"
        if self.optionType != "" {
            returnStr = self.optionType
        }
        return returnStr
    }
    
    //returns complete Dictionary of options and their prices
    func returnOptions() ->[String:Double]{
        var returnData = ["error":0.0]
        if options.count > 0 {
            returnData = options
        }
        return returnData
    }
    
    
    
}
